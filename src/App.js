import React from "react";
import "./css/App.css";
import "normalize.css";
import Main from "./pages/main/index";

export default function App() {
  return (
    <div className="App">
      <Main />
    </div>
  );
}
