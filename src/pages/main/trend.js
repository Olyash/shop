import React from "react";
import "../../css/main/trend.css";

export default function Trend() {
  return (
    <div className="trend">
      <div className="container trend-common">
        <h1 className="trend-text">
          IT IS NOT ABOUT BRAND <br />
          IT IS ABOUT STYLE
        </h1>
        <img
          src={require("../../static/img/trend-circle.png")}
          alt="circle"
          className="trend-img-circle"
        />
        <button className="trend-button">shop the trend</button>
      </div>
    </div>
  );
}
