import React from "react";
import "../../css/main/main.css";
import Header from "./header";
import Trend from "./trend";

export default function Main() {
  return (
    <div className="main-page">
      <Header />
      <Trend />
    </div>
  );
}
