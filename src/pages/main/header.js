import React from "react";
import "../../css/main/header.css";

export default function Header() {
  return (
    <div className="header">
      <div className="container header-grid">
        <img
          src={require("../../static/img/header-logo.png")}
          alt="logo"
          className="header-logo"
        />
        <div className="header-nav">
          <a href="" className="header-nav__link">
            home
          </a>
          <a href="" className="header-nav__link">
            catalog
          </a>
          <a href="" className="header-nav__link">
            sale
          </a>
          <a href="" className="header-nav__link">
            help
          </a>
          <a href="" className="header-nav__link">
            address
          </a>
        </div>
        <div className="header-cart">
          <img
            src={require("../../static/img/header-cart.png")}
            alt="cart"
            className="header-cart-image"
          />
          <span className="header-cart-count">3</span>
        </div>
      </div>
    </div>
  );
}
